using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    //VARIAS INTERFASES
    interface IFirstInterface
    {
        void myMethod(); // método de la interface
    }

    interface ISecondInterface
    {
        void myOtherMethod(); // método de la interface
    }

    // Implementado múltiples interfaces
    class DemoClass : IFirstInterface, ISecondInterface
    {
        public void myMethod()
        {
            Console.WriteLine("Some text..");
        }
        public void myOtherMethod()
        {
            Console.WriteLine("Some other text...");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            DemoClass myObj = new DemoClass();
            myObj.myMethod();
            myObj.myOtherMethod();


            Console.Read();
        }
    }
}

